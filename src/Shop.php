<?php

namespace Laba;

class Shop
{
    /**
     * Коллекция созданных магазинов
     * @var array | Shop
     */
    private static $collection = [];

    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $address;

    /**
     * Приватный конструктор, чтобы можно было контролировать создание объектов с одинаковыми id и гарантированно помещать в коллекции
     * Shop constructor.
     * @param int $id
     * @param string $name
     * @param string $address
     */
    private function __construct(int $id, string $name, string $address)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
    }

    public static function create(int $int, string $name, string $address)
    {
        foreach (self::$collection as $newShop){
            if($newShop->getId() === $int) throw new \Error();
        }

        $newShop = new static($int, $name, $address);
        self::$collection[] = $newShop;
        return $newShop;

    }

    public static function findAll(): array
    {
        return self::$collection;
    }

    /**
     * Отчистить коллекцию
     */
    public static function clearCollection(): void
    {
        self::$collection = [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }
}