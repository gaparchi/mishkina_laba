<?php

namespace Tests;

use Laba\Shop;
use PHPUnit\Framework\TestCase;

class ShopTest extends TestCase
{
    /**
     * Метод выполняется перед каждым, тестом.
     * Отчищает коллекцию магазинов
     */
    protected function setUp(): void
    {
        Shop::clearCollection();
    }

    public  function testCreateShop_byNew_expectedException()
    {
        self::expectException(\Error::class);
        new Shop(100, 'Магазин', 'адрес');
    }

    public function testCreateShop_byFactoryMethod()
    {
        $shop = Shop::create(100, 'Магазин', 'адрес');

        self::assertIsObject($shop);
        self::assertInstanceOf(Shop::class, $shop);

        self::assertSame(100, $shop->getId());
        self::assertSame('Магазин', $shop->getName());
        self::assertSame('адрес', $shop->getAddress());
    }

    public function testFindAll()
    {
        $shopCount = 5;
        for($i=0 ; $i<$shopCount; $i++){
            Shop::create($i, 'Магазин'.$i, 'адрес'.$i);
        }

        self::assertCount($shopCount, Shop::findAll());
        self::assertContainsOnlyInstancesOf(Shop::class, Shop::findAll());
    }

    public function testCreate_dublicateId_exception()
    {
        Shop::create(100, 'Магазин', 'адрес');

        self::expectException(\Error::class);
        Shop::create(100, 'Еще одни магазин с одинаковым Id', 'адрес');
    }
}